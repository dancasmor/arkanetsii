package pgpi_arkanoid;

public abstract class GameObject {
	
	/* 
	Posiciones X en las que se situan los dos lados verticales 
	del rectangulo que cubre el objeto:
	*/
	abstract double left();

	abstract double right();

	/* 
	Posiciones Y en las que se situan los dos lados horizontales 
	del rectangulo que cubre el objeto:
	*/
	abstract double top();

	abstract double bottom();
}
